#!/bin/bash

cd $(dirname $0)

if [ -n "$1" ]; then
    CLUSTER_NAME=$1
else
    CLUSTER_NAME="my-app-cluster"
fi
echo "Create $CLUSTER_NAME Cluster!!"

kind create cluster --config ../../manifest/platform/kind/kind.yaml --name $CLUSTER_NAME

kubectl cluster-info --context "kind-$CLUSTER_NAME"
