#!/bin/bash

if [ -n "$1" ]; then
    CLUSTER_NAME=$1
else
    CLUSTER_NAME="my-app-cluster"
fi
echo "Delete $CLUSTER_NAME Cluster!!"

cd $(dirname $0)
kind delete cluster --name $CLUSTER_NAME
