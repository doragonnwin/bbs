#!/bin/bash

echo "Setting $(kubectl config current-context) Cluster!!"

cd $(dirname $0)

# calicoのデプロイ
kubectl apply -f ../../manifest/platform/calico/tigera-calico.yaml
kubectl apply -f ../../manifest/platform/calico/custom-resources.yaml

# metallbのデプロイ
kustomize build ../../manifest/platform/metallb | kubectl apply -f -

# NGINX Ingress Controllerのデプロイ
kustomize build ../../manifest/platform/ingress-nginx | kubectl apply -f -

# metrics-serverのデプロイ
kubectl apply -f ../../manifest/platform/metrics-server/components.yaml
