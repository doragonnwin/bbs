KIND_CLUSTER_NAME := my-app-cluster
UNIT_TEST_DIR := ./out/unitTest
LINT_DIR := ./out/lint

.PHONY: create-cluster
create-cluster:
	./tools/kind/create-cluster.sh $(KIND_CLUSTER_NAME)
	./tools/kind/setting-cluster.sh

.PHONY: delete-cluster
delete-cluster:
	./tools/kind/delete-cluster.sh $(KIND_CLUSTER_NAME)

.PHONY: db
db:
	kubectl delete deploy -n db --ignore-not-found app
	kustomize build manifest/platform/db | kubectl apply -f -

.PHONY: board
board:
	kubectl delete deploy -n board --ignore-not-found app
	docker build -t doragonnwin/bbs/board:latest --file ./app/board/Dockerfile .
	kind load docker-image doragonnwin/bbs/board:latest --name $(KIND_CLUSTER_NAME)
	kustomize build manifest/app/base/board | kubectl apply -f -

.PHONY: test
test:
	go clean -testcache
	mkdir -p $(UNIT_TEST_DIR)
	go test ./... -v -coverprofile $(UNIT_TEST_DIR)/cover.out.tmp >$(UNIT_TEST_DIR)/result.out 2>&1; \
	EXIT=$$; \
	cat $(UNIT_TEST_DIR)/cover.out.tmp | grep -v "_gen.go" > $(UNIT_TEST_DIR)/cover.out; \
	go tool cover -html $(UNIT_TEST_DIR)/cover.out -o $(UNIT_TEST_DIR)/cover.html; \
	exit $(EXIT)

.PHONY: lint
lint:
	mkdir -p $(LINT_DIR)
	golangci-lint run -c .golangci.yml > $(LINT_DIR)/lint-result.json
