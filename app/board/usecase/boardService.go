package usecase

import (
	"context"

	"gitlab.com/doragonnwin/bbs/app/board/presentation/proto"
	"go.uber.org/zap"
)

//BoardService usecase層構造体
type BoardService struct {
	logger *zap.Logger
}

//NewBoardService usecase層コンストラクタ
func NewBoardService(l *zap.Logger) *BoardService {
	return &BoardService{logger: l}
}

//CreateMessage メッセージ作成
func (bs *BoardService) CreateMessage(ctx context.Context, req *proto.CreateMessageRequest) (*proto.CreateMessageResponse, error) {
	return &proto.CreateMessageResponse{Id: "0"}, nil
}
