// +build wireinject

package main

import (
	pkgwire "github.com/google/wire"
	"gitlab.com/doragonnwin/bbs/app/board/presentation/proto"
	"gitlab.com/doragonnwin/bbs/app/board/usecase"
	"gitlab.com/doragonnwin/bbs/common/logger"
)

//InitializeBoard DI関数
func InitializeBoard() *BoardPresentation {
	pkgwire.Build(
		logger.New,
		NewBoardPresentation,
		usecase.NewBoardService,
		pkgwire.Bind(new(proto.BoardServiceServer), new(*usecase.BoardService)))

	return &BoardPresentation{}
}
