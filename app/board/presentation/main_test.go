package main

import (
	"context"
	"reflect"
	"testing"
	"time"

	"gitlab.com/doragonnwin/bbs/app/board/presentation/proto"
	"gitlab.com/doragonnwin/bbs/common/logger"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

func TestNewBoardPresentation(t *testing.T) {
	l := logger.New()
	mbs := NewBoardServiceServerMock()

	type args struct {
		bs proto.BoardServiceServer
		l  *zap.Logger
	}
	tests := []struct {
		name string
		args args
		want *BoardPresentation
	}{
		{
			name: "正常系",
			args: args{bs: mbs, l: l},
			want: &BoardPresentation{BoardService: mbs, logger: l},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBoardPresentation(tt.args.bs, tt.args.l); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBoardPresentation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBoardPresentation_run(t *testing.T) {
	mbs := NewBoardServiceServerMock()
	l := logger.New()

	type fields struct {
		BoardService proto.BoardServiceServer
		logger       *zap.Logger
	}
	tests := []struct {
		name        string
		fields      fields
		wantMessage string
	}{
		{
			name:        "正常系",
			fields:      fields{BoardService: mbs, logger: l},
			wantMessage: "unitTest",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			bp := &BoardPresentation{
				BoardService: tt.fields.BoardService,
				logger:       tt.fields.logger,
			}
			go func() {
				bp.run()
			}()

			//少し待たないとbp.run()中のlistenが始まらずにエラーになる
			time.Sleep(1 * time.Millisecond)
			conn, _ := grpc.DialContext(context.Background(), ":5000", grpc.WithInsecure())
			defer conn.Close()

			client := proto.NewBoardServiceClient(conn)
			gotMessage, _ := client.CreateMessage(context.Background(), &proto.CreateMessageRequest{})
			if gotMessage.Id != tt.wantMessage {
				t.Errorf("want:%v ,got:%v", tt.wantMessage, gotMessage.Id)
			}

		})
	}
}

type BoardServiceServerMock struct {
}

func NewBoardServiceServerMock() *BoardServiceServerMock {
	return &BoardServiceServerMock{}
}

func (bs *BoardServiceServerMock) CreateMessage(ctx context.Context, req *proto.CreateMessageRequest) (*proto.CreateMessageResponse, error) {
	return &proto.CreateMessageResponse{Id: "unitTest"}, nil
}
