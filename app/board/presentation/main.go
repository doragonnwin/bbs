package main

import (
	"context"
	"os"
	"os/signal"

	"gitlab.com/doragonnwin/bbs/app/board/presentation/proto"
	cmngrpc "gitlab.com/doragonnwin/bbs/common/grpc"
	"go.uber.org/zap"
	"golang.org/x/sys/unix"
	"google.golang.org/grpc"
)

const (
	port = 5000 //grpc待受ポート
)

//BoardPresentation Presentation層構造体
type BoardPresentation struct {
	BoardService proto.BoardServiceServer
	logger       *zap.Logger
}

//NewBoardPresentation Presentation層コンストラクタ
func NewBoardPresentation(bs proto.BoardServiceServer, l *zap.Logger) *BoardPresentation {
	return &BoardPresentation{
		BoardService: bs,
		logger:       l,
	}
}

func main() {
	bp := InitializeBoard()
	os.Exit(bp.run())
}

//run サーバ起動
func (bp *BoardPresentation) run() int {
	//SIGTERM,SIGINTを受け取るように設定
	ctx, stop := signal.NotifyContext(context.Background(), unix.SIGTERM, unix.SIGINT)
	defer stop()

	//サーバをgoルーチンで実行
	errCh := make(chan error, 1)
	go func() {
		errCh <- cmngrpc.NewServer(port, bp.logger, func(s *grpc.Server) {
			proto.RegisterBoardServiceServer(s, bp.BoardService)
		}).Start(ctx)
	}()

	select {
	//サーバにエラーが起きた場合
	case err := <-errCh:
		bp.logger.Error("error shutting down...", zap.Error(err))
		return 1
	//SIGTERM,SIGINTを受け取った場合
	case <-ctx.Done():
		bp.logger.Info("shutting down...")
		return 0
	}
}
