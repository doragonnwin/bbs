package interceptor

import (
	"context"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
)

func NewRequestLogger(logger *zap.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		logger = logger.With(zap.String("method", info.FullMethod))
		logger.Info("grpc request start")

		res, err := handler(ctx, req)

		logger.Info("grpc request finish", zap.Any("code", status.Code(err)))

		return res, err
	}
}
