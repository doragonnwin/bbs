package logger

import (
	"bytes"
	"os"
	"strings"
	"testing"

	"go.uber.org/zap"
)

func TestNew(t *testing.T) {

	tests := []struct {
		name         string
		inputMessage string
		wantMessage  string
		wantErr      bool
	}{
		{
			name:         "正常系",
			inputMessage: "unitTest",
			wantMessage:  "unitTest",
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertLogger(t, tt.inputMessage, tt.wantMessage, New)
		})
	}
}

func assertLogger(t *testing.T, inMsg string, wantMsg string, fnc func() *zap.Logger) {
	t.Helper()

	//出力先の切り替え
	backup := os.Stdout
	defer func() {
		os.Stdout = backup
	}()
	r, w, err := os.Pipe()
	if err != nil {
		t.Fatalf("fail pipe: %v", err)
	}
	os.Stdout = w

	//ロガーの生成とログ出力
	logger := fnc()
	logger.Info(inMsg)
	logger.Warn(inMsg)
	logger.Debug(inMsg)
	logger.Error(inMsg)

	//書き込まれたものの読み込み
	w.Close()
	var buffer bytes.Buffer
	if n, err := buffer.ReadFrom(r); err != nil {
		t.Fatalf("fail read buf: %v - number: %v", err, n)
	}

	//アサーション
	s := strings.Split(buffer.String(), "\n")
	gotMessages := s[:len(s)-1]
	for _, gotMessage := range gotMessages {
		if strings.Contains(gotMessage, "\"message\":"+wantMsg) {
			t.Errorf("gotMessage: %v", gotMessage)
			t.Errorf("wantMessage: %v", wantMsg)
		}
	}

}
